import Foundation

public protocol ViewConfigurable {
    associatedtype ViewData
    func configure(with viewData: ViewData)
}
