import UIKit

public struct Style: Equatable {
    
    public struct Font: Equatable {
        public let title: UIFont
        public let body: UIFont
        public let link: UIFont
    }
    
    public struct Color: Equatable {
        public let title: UIColor
        public let body: UIColor
        public let link: UIColor
        public let border: UIColor
    }
    
    public let font: Font
    public let color: Color
    public let borderWidth: CGFloat
}

extension Style {
    public static let defaultStyle: Style = Style(
        font: Style.Font(
            title: .systemFont(ofSize: 22),
            body: .systemFont(ofSize: 18),
            link: .systemFont(ofSize: 16)
        ),
        color: Style.Color(
            title: .black,
            body: .darkGray,
            link: .systemBlue,
            border: .lightGray
        ),
        borderWidth: 0.5
    )
}
