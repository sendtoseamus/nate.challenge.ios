import UIKit
import UI
import NetworkingService
import ProductsList

final class AppCoordinator {
    
    private let navigationController: UINavigationController
    private let service: Service
    private let style: Style

    private var productsCoordinator: ProductsListCoordinator?
    
    init(
        navigationController: UINavigationController,
        service: Service = Service(),
        style: Style = Style.defaultStyle
    ) {
        self.navigationController = navigationController
        self.service = service
        self.style = style
    }
    
    func start() {
        productsCoordinator = ProductsListCoordinator(
            navigationController: navigationController,
            service: service,
            style: style
        )
        productsCoordinator?.start()
    }
}
