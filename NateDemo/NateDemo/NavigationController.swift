import UIKit

final class NavigationController: UINavigationController {
    
    private var appCoordinator: AppCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appCoordinator = AppCoordinator(navigationController: self)
        appCoordinator?.start()
    }
}
