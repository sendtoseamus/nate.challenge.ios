import XCTest
import UI
import NetworkingService
@testable import ProductsList

final class ProductsListViewModelTests: XCTestCase {
        
    func testDidLoadURL() {
        let service = ServiceSpy()
        let sut = ProductsListViewModel(service: service, style: Style.defaultStyle)
        sut.viewDidLoad()
        
        XCTAssertEqual(service.url, "http://localhost:3000/products")
    }
    
    func testDidLoadViewData() {
        let service = ServiceSpy()
        service.response = ProductsResponse.testResponse
        
        let sut = ProductsListViewModel(service: service, style: Style.defaultStyle)
        sut.viewDidLoad()
        
        XCTAssertEqual(
            sut.viewData,
            ProductsListViewController.ViewData(
                title: "Products",
                products: [
                    ProductTableViewCell.ViewData(
                        title: "test merchant",
                        body: "test product",
                        imageUrl: URL(string: "https://fanatics.frgimages.com/FFImage/thumb.aspx?i=/productimages/_3802000/ff_3802240-4b85edcce601a42d6641_full.jpg"),
                        style: Style.defaultStyle
                    )
                ]
            )
        )
    }
}

extension ProductsResponse {
    static var testResponse: ProductsResponse {
        ProductsResponse(
            posts: [
                Post(
                    id: "123",
                    title: "test product",
                    merchant: "test merchant",
                    images: ["https://fanatics.frgimages.com/FFImage/thumb.aspx?i=/productimages/_3802000/ff_3802240-4b85edcce601a42d6641_full.jpg"]
                )
            ]
        )
    }
}

final class ServiceSpy: ServiceProvider {
    var url: String?
    var response: Any?
    var error: Error?
    
    func load<T>(_ api: Api, completion: @escaping (T?, Error?) -> Void) -> Request? where T : Decodable {
        self.url = api.url
        
        if let response = response as? T {
            completion(response, nil)
        
        } else {
            completion(nil, error)
        }
        
        return nil
    }
}
