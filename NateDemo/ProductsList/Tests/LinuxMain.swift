import XCTest

import ProductsListTests

var tests = [XCTestCaseEntry]()
tests += ProductsListTests.allTests()
XCTMain(tests)
