import UIKit
import UI
import NetworkingService

final class ProductsListViewModel: NSObject {
    
    private let service: ServiceProvider
    private var request: Request?
    private let style: Style
    private var productsResponse: ProductsResponse?
    
    private var productPosts: [Post] {
        guard let response = productsResponse else {
            return []
        }
        return response.posts
    }
    
    var reloadView: ((ProductsListViewController.ViewData) -> Void)?
    
    var disSelect: ((Post) -> Void)?
    
    init(service: ServiceProvider, style: Style) {
        self.service = service
        self.style = style
    }
    
    var viewData: ProductsListViewController.ViewData {
        
        ProductsListViewController.ViewData(
            title: "Products",
            products: productPosts.map {                
                ProductTableViewCell.ViewData(
                    title: $0.merchant,
                    body: $0.title,
                    imageUrl: $0.firstImageURL,
                    style: style
                )
            }
        )
    }
        
    func viewDidLoad() {
        
        request = service.load(
            ProductListApi(),
            completion: { [weak self] (products: ProductsResponse?, error: Error?) in
                guard let self = self else { return }

                if let products = products {
                    self.productsResponse = products
                }
                self.reloadView?(self.viewData)
            }
        )
    }
}

extension ProductsListViewModel: UITableViewDataSource, UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewData.products.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell") as! ProductTableViewCell

        if indexPath.row <= viewData.products.count {
            let product = viewData.products[indexPath.row]
            
            cell.configure(
                with: ProductTableViewCell.ViewData(
                    title: product.title,
                    body: product.body,
                    imageUrl: product.imageUrl,
                    style: style
                )
            )
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row <= productPosts.count else { return }
        let post = productPosts[indexPath.row]
        disSelect?(post)
    }
}
    
