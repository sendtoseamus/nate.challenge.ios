import UIKit
import UI
import ProductDetails
import NetworkingService

public final class ProductsListCoordinator {

    private let service: ServiceProvider
    private let navigationController: UINavigationController
    private let style: Style
    private var viewModel: ProductsListViewModel?
    
    public init(
        navigationController: UINavigationController,
        service: ServiceProvider,
        style: Style
    ) {
        self.navigationController = navigationController
        self.service = service
        self.style = style
    }
    
    public func start() {
        
        if let productsVc = ProductsListViewController.make() {
            productsVc.viewModel = ProductsListViewModel(service: service, style: style)
            navigationController.setViewControllers([productsVc], animated: false)
            
            viewModel = productsVc.viewModel
            viewModel?.disSelect = { [weak self] post in
                self?.load(post: post)
            }
        }
    }
    
    private func load(post: Post) {
        
        let productDetails = ProductDetailsCoordinator(
            navigationController: navigationController,
            service: service,
            productId: post.id,
            style: style
        )
        productDetails.start()
    }
}
