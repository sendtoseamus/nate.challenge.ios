import Foundation
import NetworkingService

struct ProductListApi: Api {
    let url = "http://localhost:3000/products"
    let method = Method.POST
}
