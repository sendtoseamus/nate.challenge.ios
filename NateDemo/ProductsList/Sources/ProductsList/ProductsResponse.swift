import Foundation

struct ProductsResponse: Decodable {
    let posts: [Post]
    
    init(posts: [Post]) {
        self.posts = posts
    }
}

struct Post: Decodable {
    let id: String
    let title: String
    let merchant: String
    let images: [String]
    
    init(id: String, title: String, merchant: String, images: [String]) {
        self.id = id
        self.title = title
        self.merchant = merchant
        self.images = images
    }
}

extension Post {
    var firstImageURL: URL? {
        guard let image = images.first else {
            return nil
        }
       return URL(string: image)
    }
}
