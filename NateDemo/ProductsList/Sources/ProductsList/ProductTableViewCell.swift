import UIKit
import UI
import MapleBacon

final class ProductTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        productImageView.layer.borderColor = UIColor.lightGray.cgColor
        productImageView.layer.borderWidth = 0.5
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        title.text = nil
        body.text = nil
        productImageView.image = nil
    }
}

extension ProductTableViewCell: ViewConfigurable {
    struct ViewData: Equatable {
        let title: String
        let body: String
        let imageUrl: URL?
        let style: Style
    }
    
    func configure(with viewData: ViewData) {
        title.text = viewData.title
        body.text = viewData.body
        productImageView?.setImage(with: viewData.imageUrl)
    }
}
