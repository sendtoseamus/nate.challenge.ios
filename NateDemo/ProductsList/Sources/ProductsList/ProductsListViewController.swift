import UIKit
import UI

final class ProductsListViewController: UIViewController {
    
    static func make() -> ProductsListViewController? {
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ProductsListViewController") as? ProductsListViewController
    }
    
    @IBOutlet private weak var tableView: UITableView!
    
    var viewModel: ProductsListViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = viewModel
        tableView.dataSource = viewModel
        
        viewModel?.reloadView = { [weak self] viewData in
            self?.configure(with: viewData)
        }
        
        viewModel?.viewDidLoad()
    }
}

extension ProductsListViewController: ViewConfigurable {
    struct ViewData: Equatable {        
        let title: String
        let products: [ProductTableViewCell.ViewData]
    }

    func configure(with viewData: ViewData) {
        title = viewData.title
        tableView.reloadData()
    }
}
