// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ProductsList",
    platforms: [.iOS(.v10)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "ProductsList",
            targets: ["ProductsList"]),
    ],
    dependencies: [
        .package(path: "../../NetworkingService"),
        .package(path: "../../ProductDetails"),
        .package(path: "../../UI"),
        .package(url: "https://github.com/JanGorman/MapleBacon.git", from: Version("6.1.1"))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "ProductsList",
            dependencies: ["NetworkingService", "ProductDetails", "UI", "MapleBacon"]),
        .testTarget(
            name: "ProductsListTests",
            dependencies: ["ProductsList"]),
    ]
)
