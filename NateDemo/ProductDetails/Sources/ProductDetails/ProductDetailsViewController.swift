import UIKit
import UI

final class ProductDetailsViewController: UIViewController {
    
    static func make() -> ProductDetailsViewController? {
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ProductDetailsViewController") as? ProductDetailsViewController
    }
    
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var productUrl: UITextView!

    var viewModel: ProductDetailsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.delegate = viewModel
        collectionView.dataSource = viewModel

        viewModel?.reloadView = { [weak self] viewData in
            guard let viewData = viewData else { return }
            self?.configure(with: viewData)
        }        
        viewModel?.viewDidLoad(screenWidth: view.bounds.width)
    }
}

extension ProductDetailsViewController: ViewConfigurable {
    
    struct ViewData: Equatable {
        let title: String
        let body: String
        let images: [String]
        let url: String
        let style: Style
    }
    
    func configure(with viewData: ViewData) {
        title = viewData.title

        collectionView.reloadData()
        collectionView.layer.borderColor = viewData.style.color.border.cgColor
        collectionView.layer.borderWidth = viewData.style.borderWidth

        productTitle.text = viewData.title
        productTitle.font = viewData.style.font.title
        productTitle.textColor = viewData.style.color.title
        
        body.text = viewData.body
        body.font = viewData.style.font.body
        body.textColor = viewData.style.color.body
        
        productUrl.text = viewData.url
        productUrl.font = viewData.style.font.link
        productUrl.textColor = viewData.style.color.link
    }
}
