import UIKit
import UI
import MapleBacon

final class ProductImageCollectionViewCell: UICollectionViewCell {    
    @IBOutlet private weak var productImageView: UIImageView!
}

extension ProductImageCollectionViewCell: ViewConfigurable {
    
    struct ViewData {
        let imageUrl: String
    }
    
    func configure(with viewData: ViewData) {
        productImageView.setImage(with: URL(string: viewData.imageUrl))
    }
}
