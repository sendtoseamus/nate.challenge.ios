import Foundation

struct ProductDetailsResponse: Decodable {
    let post: Post
    
    init(post: Post) {
        self.post = post
    }
}

struct Post: Decodable {
    let title: String
    let merchant: String
    let images: [String]
    let url: String
    
    init(title: String, merchant: String, images: [String], url: String) {
        self.title = title
        self.merchant = merchant
        self.images = images
        self.url = url
    }
}

extension Post {
    var firstImageURL: URL? {
        guard let image = images.first else {
            return nil
        }
       return URL(string: image)
    }
}
