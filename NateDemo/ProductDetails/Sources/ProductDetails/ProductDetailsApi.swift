import Foundation
import NetworkingService

struct ProductDetailsApi: Api {
    let url: String
    let method = Method.GET
    
    init(productId: String) {
        self.url = "http://localhost:3000/product/\(productId)"
    }
}
