import UIKit
import UI
import NetworkingService

public final class ProductDetailsCoordinator {

    private let service: ServiceProvider
    private let navigationController: UINavigationController
    private let productId: String
    private let style: Style
    private var viewModel: ProductDetailsViewModel?
    
    public init(
        navigationController: UINavigationController,
        service: ServiceProvider,
        productId: String,
        style: Style
    ) {
        self.navigationController = navigationController
        self.service = service
        self.productId = productId
        self.style = style
    }
    
    public func start() {
        guard let detailsVc = ProductDetailsViewController.make() else { return }        
        viewModel = ProductDetailsViewModel(service: service, productId: productId, style: style)
        detailsVc.viewModel = viewModel
        
        navigationController.pushViewController(detailsVc, animated: true)
    }
}
