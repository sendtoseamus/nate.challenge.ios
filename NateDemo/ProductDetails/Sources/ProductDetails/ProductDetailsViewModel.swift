import Foundation
import UIKit
import UI
import NetworkingService

final class ProductDetailsViewModel: NSObject {
    
    private let service: ServiceProvider
    private var request: Request?
    private let productId: String
    private let style: Style
    private var productResponse: ProductDetailsResponse?
    private var screenWidth: CGFloat = 0
        
    var reloadView: ((ProductDetailsViewController.ViewData?) -> Void)?
    
    var viewData: ProductDetailsViewController.ViewData? {
        guard let response = productResponse else {
            return nil
        }
        return ProductDetailsViewController.ViewData(
            title: response.post.merchant,
            body: response.post.title,
            images: response.post.images,
            url: response.post.url,
            style: style
        )
    }
    
    init(
        service: ServiceProvider,
        productId: String,
        style: Style
    ) {
        self.service = service
        self.productId = productId
        self.style = style
    }
    
    func viewDidLoad(screenWidth: CGFloat) {
        request = service.load(
            ProductDetailsApi(productId: productId),
            completion: { [weak self] (product: ProductDetailsResponse?, error: Error?) in
                guard let self = self else { return }
                self.productResponse = product
                self.reloadView?(self.viewData)
            }
        )
        self.screenWidth = screenWidth
    }
}

extension ProductDetailsViewModel: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let viewData = viewData else { return 0 }
        return viewData.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductImageCollectionViewCell", for: indexPath) as! ProductImageCollectionViewCell
        
        if let viewData = viewData, indexPath.row <= viewData.images.count {
            cell.configure(with: ProductImageCollectionViewCell.ViewData(
                    imageUrl: viewData.images[indexPath.row]
                )
            )
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: screenWidth, height: screenWidth)
    }
}
