import XCTest
import UI
import NetworkingService
@testable import ProductDetails

final class ProductDetailsTests: XCTestCase {

    func testDidLoadURL() {
        let service = ServiceSpy()
        let sut = ProductDetailsViewModel(
            service: service,
            productId: "123",
            style: Style.defaultStyle
        )
        sut.viewDidLoad(screenWidth: 414)
        
        XCTAssertEqual(service.url, "http://localhost:3000/product/123")
    }
    
    func testDidLoadViewData() {
        let service = ServiceSpy()
        service.response = ProductDetailsResponse.testResponse
        let sut = ProductDetailsViewModel(
            service: service,
            productId: "123",
            style: Style.defaultStyle
        )
        sut.viewDidLoad(screenWidth: 414)

        XCTAssertEqual(
            sut.viewData,
            ProductDetailsViewController.ViewData(
                title: "test merchant",
                body: "test product",
                images: ["https://fanatics.frgimages.com/FFImage/thumb.aspx?i=/productimages/_3802000/ff_3802240-4b85edcce601a42d6641_full.jpg"],
                url: "https://embrlabs.com/products/embr-wave",
                style: Style.defaultStyle
            )
        )
    }
}

extension ProductDetailsResponse {
    static var testResponse: ProductDetailsResponse {
        ProductDetailsResponse(
            post: Post(
                title: "test product",
                merchant: "test merchant",
                images: ["https://fanatics.frgimages.com/FFImage/thumb.aspx?i=/productimages/_3802000/ff_3802240-4b85edcce601a42d6641_full.jpg"],
                url: "https://embrlabs.com/products/embr-wave"
            )
        )
    }
}

final class ServiceSpy: ServiceProvider {
    var url: String?
    var response: Any?
    var error: Error?
    
    func load<T>(_ api: Api, completion: @escaping (T?, Error?) -> Void) -> Request? where T : Decodable {
        self.url = api.url
        
        if let response = response as? T {
            completion(response, nil)
        
        } else {
            completion(nil, error)
        }
        
        return nil
    }
}
