import XCTest

import ProductDetailsTests

var tests = [XCTestCaseEntry]()
tests += ProductDetailsTests.allTests()
XCTMain(tests)
