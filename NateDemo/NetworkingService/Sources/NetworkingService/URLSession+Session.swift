import Foundation

public protocol Session {
    func dataTask(
        with request: URLRequest,
        completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void
    ) -> URLSessionDataTask
    
    var configuration: URLSessionConfiguration { get }
}

extension URLSession: Session { }
