import Foundation

public protocol ServiceProvider {
    func load<T: Decodable>(
        _ api: Api,
        completion: @escaping (T?, Error?) -> Void
    ) -> Request?
}

public final class Service: ServiceProvider {
        
    private let session: Session
    
    public init(
        session: Session = URLSession(configuration: .default)
    ) {
        session.configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        self.session = session
    }

    public func load<T: Decodable>(
        _ api: Api,
        completion: @escaping (T?, Error?) -> Void
    ) -> Request? {
                
        let request = Request(api: api, session: session) { [weak self] (data, response, error) in

            if let error = error {
                print(error.localizedDescription)
            
            } else if let response = response {
                print(response.description)
            }
            let result = try? self?.decode(T.self, from: data)
            
            DispatchQueue.main.async {
                completion(result, error)
            }
        }
        request.start()
        
        return request
    }

    private func decode<T: Decodable>(_ type: T.Type, from data: Data?) throws -> T? {
        guard let data = data else { return nil }
        let jsonDecoder: JSONDecoder = JSONDecoder()
        let result: T? = try jsonDecoder.decode(T.self, from: data)
        return result
    }
}
