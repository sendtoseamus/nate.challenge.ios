import Foundation

public final class Request {
    public typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void
    
    private let session: Session
    private let api: Api
    private let completionHandler: CompletionHandler
    
    public init(
        api: Api,
        session: Session,
        completion: @escaping CompletionHandler
    ) {
        self.api = api
        self.session = session
        self.completionHandler = completion
    }
    
    func start() {
        guard let url = URL(string: api.url) else { return }
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        urlRequest.httpMethod = api.method.rawValue
        api.headers.forEach {
            urlRequest.setValue($0.value, forHTTPHeaderField: $0.key)
        }

        session.dataTask(with: urlRequest) { [weak self] (data, response, error) in
            self?.completionHandler(data, response, error)
            
        }.resume()
    }
}
