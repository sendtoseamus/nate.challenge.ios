import Foundation

public enum Method: String {
    case POST
    case GET
}

public protocol Api {
    var url: String { get }
    var method: Method { get }
    var headers: [String: String] { get }
}

public extension Api {
    var defaultHeaders: [String: String] {
        ["Content-Type": "application/json"]
    }
    
    var headers: [String: String] { defaultHeaders }
}
