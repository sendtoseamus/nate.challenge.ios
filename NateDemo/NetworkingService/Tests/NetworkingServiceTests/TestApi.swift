import Foundation
@testable import NetworkingService

struct TestApi: Api {
    let url = "http://test.com/items"
    var method = Method.POST
}

