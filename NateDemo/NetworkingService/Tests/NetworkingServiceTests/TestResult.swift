import Foundation

struct TestResult {
    
    init(
        data: Data? = nil,
        response: URLResponse? = nil,
        error: Error? = nil,
        decodable: Decodable? = nil
    ) {
        self.data = data
        self.response = response
        self.error = error
        self.decodable = decodable
    }

    let data: Data?
    let response: URLResponse?
    let error: Error?
    let decodable: Decodable?
}
