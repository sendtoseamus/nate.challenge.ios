import XCTest
@testable import NetworkingService

class RequestTests: XCTestCase {
	
    func testRequestDidComplete() throws {
        let ex = expectation(description: "wait for request to complete")
        let sut = try makeSUT(
            completion: { (_, _, _) in
                ex.fulfill()
            }
        )
        sut.start()
        wait(for: [ex], timeout: 1)
    }
    
    func testRequestDidReturnResponseAndData() throws {
        let ex = expectation(description: "wait for request to complete")
        var result: TestResult?
        let url = "http://localhost:3000/products"
        let response = URLResponse(
            url: try XCTUnwrap(URL(string: url)),
            mimeType: nil,
            expectedContentLength: -1,
            textEncodingName: nil
        )
        let sut = try makeSUT(
            data: Data(),
            response: response,
            completion: { (data, response, _) in
                result = TestResult(data: data, response: response, error: nil)
                ex.fulfill()
            }
        )
        sut.start()
        wait(for: [ex], timeout: 1)
                
        XCTAssertNotNil(result?.data)
        XCTAssertNotNil(result?.response)
    }

    func testRequestDidReturnError() throws {
        let ex = expectation(description: "wait for request to complete")
        var result: TestResult?
        let sut = try makeSUT(
            error: TestError(),
            completion: { (_, _, error) in
                result = TestResult(data: nil, response: nil, error: error)
                ex.fulfill()
            }
        )
        sut.start()
        wait(for: [ex], timeout: 1)
        
        XCTAssertNotNil(result?.error)
    }
    
    private func makeSUT(
        api: Api = TestApi(),
        data: Data? = nil,
        response: URLResponse? = nil,
        error: Error? = nil,
        completion: @escaping Request.CompletionHandler
    ) throws -> Request {
        
        let session = SessionMock()
        session.result = TestResult(data: data, response: response, error: error)
        return Request(api: api, session: session, completion: completion)
    }
}
