import XCTest
@testable import NetworkingService

final class ServiceTests: XCTestCase {
    
    func testDidComplete() {
        let ex = expectation(description: "wait for session to load")
        let sut = makeSUT()
        
        _ = sut.load(TestApi(), completion: { (products: TestResponse?, error: Error?) in
            ex.fulfill()
        })
        
        wait(for: [ex], timeout: 1)
    }
    
    func testDidLoadError() {
        let ex = expectation(description: "wait for session to load")
        let sut = makeSUT(error: TestError())
        var result: TestResult?
        
        _ = sut.load(TestApi(), completion: { (products: TestResponse?, error: Error?) in
            result = (TestResult(error: error))
            ex.fulfill()
        })
        
        wait(for: [ex], timeout: 1)
        XCTAssertNotNil(result?.error)
    }
        
    func testDidLoadDecodable() throws {
        let ex = expectation(description: "wait for session to load")
        let sut = try makeSUT(data: TestResponse.makeData())
        var result: TestResult?
        
        _ = sut.load(TestApi(), completion: { (products: TestResponse?, error: Error?) in
            result = (TestResult(decodable: products))
            ex.fulfill()
        })
        
        wait(for: [ex], timeout: 1)
        XCTAssertNotNil(result?.decodable as? TestResponse)
    }
    
    private func makeSUT(
        data: Data? = nil,
        error: Error? = nil
    ) -> Service {

        let session = SessionMock()
        session.result = TestResult(data: data, response: nil, error: error)
        return Service(session: session)
    }
}
