import Foundation
import NetworkingService

final class SessionMock: Session {
    
    let urlSession = URLSession(configuration: .default)
    
    var result: TestResult?

    var configuration: URLSessionConfiguration {
        URLSessionConfiguration.default
    }

    func dataTask(
        with request: URLRequest,
        completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void
    ) -> URLSessionDataTask {
        
        completionHandler(result?.data, result?.response, result?.error)
        return urlSession.dataTask(with: request)
    }
}
