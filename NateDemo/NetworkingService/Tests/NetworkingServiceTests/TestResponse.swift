import Foundation
@testable import NetworkingService

struct TestResponse: Decodable {
    struct Item: Decodable {
        let id: String
        let name: String
    }
    
    let items: [Item]
}

extension TestResponse {
    static func makeData() throws -> Data? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        return try jsonEncoder.encode([
            "items": [
                ["id": "123", "name": "test item"],
                ["id": "321", "name": "test test"]
            ]
       ])
    }
}
