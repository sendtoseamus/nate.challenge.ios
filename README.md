# Nate Tech iOS Demo


  - nate.challenge.ios
 
  - Based on the specifications at: https://github.com/NateAI/nate.challenge.ios#natechallengeios�this is a small iOS application built upon the provided api.
  
  The app showcases: 

  - a Modular design with vertical feature frameworks for ProductList & ProductDetails

  - Swift Package Manager usage for modules

  - MVVM + Coordinator pattern�
  
  - Dependency Injection to decouple components, Mock and Test application.
  
  - a lightweight apiService implementation using Foundation.URLSession to communicate with the REST api
  
  - Basic UI /UX using UIkit
  
  - Style class definition to style UIComponents. 
   
 �See original github project for api documentation. 
